// require() 引用模組
// require and import 差異// import 編譯中執行，require同步加載
// const gulp = require('gulp');
// 搬家
// gulp.task('hello3', function (cd) {
//     // do  sometime(做啥事)
//     console.log("hello gulp 3.9.1")
//     cd()
// })



// gulp.task('copyFile',()=>{
//     // 'copyFile' 任務名稱，可自行定義
//     return gulp.src('source/pages/index.html').pipe(gulp.dest('./public'))
// })
// gulp.task():建立名為'copyFile'的任務(水管名稱)
// gulp.src():導入index.html檔案(水的來源)
// pipe():以gulp.src()導入的檔案需要做什麼處理(截獲水源所做的處理)
// gulp.dest():檔案輸出的目錄(水該從哪流出)
// ________以上3.9.1寫法  cli hello3執行______________

// https://awdr74100.github.io/2020-01-26-gulp-modular/   可參考模組設計
// module.exports :導出模組
// exports:導出模組
// require:引入模組

// const gulp = require('gulp');
// const { series } = require('gulp');

// 使用function 工作內容相關函式名稱(){} 即可建立任務，此時任務狀態Private(私人)，不會被cli偵測但僅供內部使用
// ，若是使用 export.任務名稱=工作內容相關函式名稱，將任務導出，cli馬上偵測任務狀態變成Public(公開)，最後執行 gulp 任務名稱 就可執行任務

// function  clean(cd){
//     cd();
// // 'clean()' 函數並未被導出(export),因此被認為是私有任務(private task),並且不能單獨使用
// // 它仍然可以用在'series()'組合中
// }
// 'bulid()' 函數被導出(export)了因此它是一個公開任務(public task),並且可以被gulp 命令直接调用
// 它也仍然可以用在'series()'組合中
// function build(cb) {
//     // body omitted
//     cb();
//   }

// function hello(cd){
//     // 任務內容
//     console.log("hello gulp 4.0 ,");
//     cd();
//   }
// export.任務名稱=工作內容相關函式名稱
//   exports.hello=hello;//獨立任務//公開任務
//   exports.build=build; 
//   exports.default=series(clean,build);//組合任務
//   const { series } = require('gulp'); 若是關掉這句就會報錯

// 3.9.1版本task()將函數作為註冊任務，4版本依舊可使用但是轉換為exports作為最主要註冊機制，除非遇到export不起作用情況。
// 若是需要讓task 按順序執行，使用series(同步) method(方法)  ,並行可用parallel(非同步)可利用這兩個API將各個獨立任務組合再一起

// ---------多任務執行---------

// function scss(cd) {
//     // 任務內容
//     cd();
//     console.log(4);
// }

// function pug(cb) {
//     // 任務內容
//     cb();
//     console.log(4);
//   }
// //   exports.build=build; 公開任務(Public Task)
// exports.build = gulp.series(scss,pug); //私有任務(Private Task)
// --------以下多任務執行例子------------
// 例1:
// gulp.task(
//     "compile", //任務名稱
//     gulp.series("clean", gulp.parallel("jade", "sass", "watch"))//執行順序
//   );

//   例2: 若是按照這樣方式Clean會被執行兩次  可能發生錯誤，建議一個function完成自己任務內容，最後在導出任務，所以可改寫成例3
// 只載入 Gulp 相關 API
// const { series, parallel } = require('gulp');
// const clean = function(cb) {
//   // 任務內容
//   cb();
//   console.log("rr");
// };
// const css = series(clean, function(cb) {
//   // 任務內容
//   cb();
// });
// const javascript = series(clean, function(cb) {
//   // 任務內容
//   cb();
// });
/* --- 導出任務 --- */
// exports.build = parallel(css, javascript);


// 例3:// 只載入 Gulp 相關 API
// var { series, parallel } = require('gulp');

// function clean(cb) {
//   // 任務內容
//   cb();
// }

// function css(cb) {
//   // 任務內容
//   cb();
// }

// function javascript(cb) {
//     // 在函式放入(cb) 回調函式當參數，然後在最後調用它
//   // 任務內容
//   cb();
// }

// /* --- 導出任務 --- */
// exports.build = series(clean, parallel(css, javascript));
// 導出任務=按照順序 先執行clean任務 ，接下來同時執行css,js兩個任務
// 這樣clean只會被執行一次

// 非同步完成後需執行一個callback function  通知gulp 任務已完成


const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css'); //壓縮css套件// npm install gulp-clean-css --save-dev
// npm install node-sass gulp-sass --save-dev
function minifiyCss() {
  // 第一種方式return
  return gulp
    .src('source/**/*.css')
    .pipe(cleanCSS({
      compatibility: 'ie8'
    }))
    .pipe(gulp.dest('dist/css'));
  //  第二種方式cb()
  // cb();
}
exports.qq = minifiyCss;
// function 工作內容相關函式名稱() {}
// exports.工作名稱 = 工作內容相關函式名稱
// 指令要下 工作名稱 gulp qq  
const gulpSass = require('gulp-sass');//編譯scss的套件
// function style(cb) {
//   gulp.src('source/scss/*.scss') //指定要處理的Scss檔案目錄
//     .pipe(gulpSass()) //編譯 Scss
//     // .pipe(gulpSass({
//     //   outputStyle: 'compressed'編譯出來的css是壓縮過的
//     // }))
//     .pipe(gulp.dest('./css')); //指定編譯後的CSS檔案目錄
//   cb();
// }
function sass (cb) {
  gulp
    .src('./source/scss/*.scss')
    .pipe(gulpSass())
    .pipe(gulp.dest('./dist/css'));
  cb();
};
exports.delete = sass;
// exports.watch = () => {
//   gulp
//     .watch("./source/scss/*.scss", gulp.series('delete'))
//     .on("all", (event, path, stats) => {
//       console.log(event);
//       console.log(path);
//       console.log(stats);
//     });
// };
// 可以只關聯一个任務
watch('source/*.css', css);
// 或者關聯一个任務组合
watch('source/*.js', series(clean, javascript));

// 所有事件都将被監控
watch('src/*.js', { events: 'all' }, function(cb) {
  // body omitted
  cb();
});