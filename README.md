Gulp4
參考網站:
https://ithelp.ithome.com.tw/articles/10220081
https://awdr74100.github.io/2020-01-28-gulp-upgradegulp/
https://www.gulpjs.com.cn/docs/getting-started/async-completion/

步驟1:gulp.require('') //引用模組
 const gulp = require('gulp'); 
 const { series } = require('gulp');
 目前測試exports有使用series、parallel可能需要引用上面這種寫法(官網範例)

// require:引入模組
// exports:導出模組
// module.exports :導出模組
require and import 差異 // import 編譯中執行，require同步加載
https://awdr74100.github.io/2020-01-26-gulp-modular/   可參考模組設計

// gulp.task():建立名為'copyFile'的任務(水管名稱)
// gulp.src():導入index.html檔案(水的來源)
// pipe():以gulp.src()導入的檔案需要做什麼處理(截獲水源所做的處理)
// gulp.dest():檔案輸出的目錄(水該從哪流出)

步驟2:創建任務task改用 
function 工作內容相關函式名稱(){} 即可建立任務，此時任務狀態Private(私人)，不會被cli偵測但僅供內部使用。

例1:
    function clean(){
        return
       //函數並未被導出(export),因此被認為是私有任務(private task),並且不能單獨使用
        // 它仍然可以用在'series()'組合中
    }
例2:
    function build(cd){
        函數被導出(export)了因此它是一個公開任務(public task),並且可以被gulp 命令直接调用
// 它也仍然可以用在'series()'組合中
        cd();
    }
    exports.任務名稱=工作內容相關函式名稱
    exports.build=build; (這樣已是//獨立任務//公開任務)
    exports.default=series(clean,build);(這樣//組合任務)
    
    // 3.9.1版本task()將函數作為註冊任務，4版本依舊可使用但是轉換為exports作為最主要註冊機制，除非遇到export不起作用情況。
// 若是需要讓task 按順序執行，使用series(同步)method(方法)  ,並行可用parallel(非同步)可利用這兩個API將各個獨立任務組合再一起
// --------以下多任務執行例子------------
 例1:
 gulp.task(
     "compile", //任務名稱
    gulp.series("clean", gulp.parallel("jade", "sass", "watch"))//執行順序
  );

例2: 若是按照這樣方式Clean會被執行兩次  可能發生錯誤，建議一個function完成自己任務內容，最後在導出任務，所以可改寫成例3

const { series, parallel } = require('gulp');
const clean = function(cb) {
        // 任務內容
        cb();
        console.log("rr");
      };
 const css = series(clean, function(cb) {   
        // 任務內容
        cb();
        });
 const javascript = series(clean, function(cb) {
       // 任務內容
       cb();
     });
/* --- 導出任務 --- */
// exports.build = parallel(css, javascript);


 例3:// 只載入 Gulp 相關 API
// var { series, parallel } = require('gulp');

     function clean(cb) {
       // 任務內容
      cb();
     }

    / function css(cb) {
     // 任務內容
     cb();
   }

     function javascript(cb) {
      // 在函式放入(cb) 回調函式當參數，然後在最後調用它
      // 任務內容
     cb();
   }

 /* --- 導出任務 --- */
 exports.build = series(clean, parallel(css, javascript));
// 導出任務=按照順序 先執行clean任務 ，接下來同時執行css,js兩個任務
// 這樣clean只會被執行一次

// 非同步完成後需執行一個callback function  通知gulp 任務已完成
gulp 4 task 的內部預設是（非同步）執行的：非同步執行會有一個問題，就是不曉得 task 會什麼時候結束，這時 gulp 就會出現提示訊息，說不確定 task 結束了沒有。
因此，若想讓 gulp 可以確認 task 是否已經完成，官方建議可以在 function 放入一個 cb 回調函式當參數，然後將 cb 插在最後調用它。


watch
Gulp 4 版本的 gulp.watch() API 可使用 all 作為事件名稱，代表著任何事件都將被觸發，包含新增、刪除等等，以及新增了 path 與 stats 參數，訪問參數即可得知更動檔案的相關資訊，簡單來講就是小幅加強了 gulp.watch() 函式

步驟3: